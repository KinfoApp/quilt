# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

<!-- ## [Unreleased] -->

## [0.1.6] - 2019-08-20

- actually passes in the headers from koa context into `NetworkManager`

## [0.1.5] - 2019-08-18

- logger middleware will fallback to `console` in render middleware

## 0.1.3

### Changed

- Improve error experience in development when server rendering fails [#850](https://github.com/Shopify/quilt/pull/850)

## 0.1.0

### Added

- `@shopify/react-server` package
